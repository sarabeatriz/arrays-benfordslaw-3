#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/// \file


#include <QMainWindow>
#include "qcustomplot.h"
using namespace std;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    /// \fn MainWindow::MainWindow(QWidget *parent)
    /// \brief Constructor
    explicit MainWindow(QWidget *parent = 0);

    /// \fn MainWindow::~MainWindow()
    /// \brief Destructor
    ///
    ~MainWindow();


    /// \fn void MainWindow::histo(string names[], double values[], int size, string xAxisLabel, string yAxisLabel)
    /// \~English
    /// \brief This function receives information about the ticks and
    ///        values for a bar chart, then displays it using the customPlot widget
    ///        from qcustomplot.
    /// \param names an array of strings for the names of the x-axis ticks
    /// \param values an array of doubles for the values of y-axis
    /// \param size the size of the names (and values) arrays
    /// \param xAxisLabel a string for the x-axis title
    /// \param yAxisLabel a string for the y-axis title
    /// \~Spanish
    /// \brief Esta funcion recibe informacion sobre las marcas en el eje y los valores
    /// para una grafica de barra, entonces la desplega usando el widget de qcustomplot.
    /// \param names un arreglo de cadenas de caracteres para los nombres de las marcas del el axis-x
    /// \param values un arreglo de dobles (doubles) para los valores del axis-y.
    /// \param size el tamano de los arreglos con los nombres (y valores).
    /// \param xAxisLabel una cadena de caracteres para el titulo del axis-x.
    /// \param yAxisLabel una cadena de caracteres para el titulo del axis-y.
	void histo(string names[], 
		double values[], int size, string xAxisLabel, string yAxisLabel);


private:
    Ui::MainWindow *ui;

    QCustomPlot *myPlot;   
};

#endif // MAINWINDOW_H
